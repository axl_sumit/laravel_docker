# BRANCH_NAME := your_branch_name
# REMOTE_NAME := origin

.PHONY: delete-local-branch

delete-local-branch:
	@git fetch --prune $(REMOTE_NAME)
	@if git branch -r | grep -q "$(REMOTE_NAME)/$(BRANCH_NAME)"; then \
		git branch -d $(BRANCH_NAME) || true; \
		echo "Local branch $(BRANCH_NAME) deleted."; \
	else \
		echo "Local branch $(BRANCH_NAME) not found."; \
	fi
