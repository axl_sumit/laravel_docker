#!/bin/bash

# Fetch the remote repo
git fetch origin

# Get the list of all local branches
local_branches=$(git branch | grep -v "*")

# Loop over all local branches
for branch in $local_branches; do
 # Check if the branch exists on the remote
 if git show-ref --verify --quiet refs/heads/$branch; then
   echo "Branch $branch exists on remote"
 else
   # Branch does not exist on remote, delete it locally
   git branch -d $branch
   echo "Deleted $branch locally"
 fi
done